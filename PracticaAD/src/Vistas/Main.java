package Vistas;

import Modelo.Conexion;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.lang.model.element.Element;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ParserConfigurationException, SAXException {
        String fichero ="C:\\Users\\carlo\\Documents\\NetBeansProjects\\PracticaAD\\src\\Vistas\\datos.xml";//Recordar cambiar la direccion del fichero al ejecutar
        //Conectamos con la BBDD
        Conexion con = new Conexion();
        Connection cone = con.getConnection();
        Statement st = cone.createStatement();
        
   //Prueba de Conexión
        try {
            int cont=1;
        
            PreparedStatement ps;
            ResultSet res;
            
            ps = cone.prepareStatement("Select * from empleado ");
            res = ps.executeQuery();
            System.out.println("Prueba de Conexión:");
             while (res.next()) {
                 System.out.println("ID: "+res.getInt("id"));
                 System.out.println("NOMBRE: "+res.getString("nombre"));
                 System.out.println("APELLIDO: "+res.getString("apellido"));
                 System.out.println("DIRECCIÓN: "+res.getString("direccion"));
                 System.out.println("TELEFONO: "+res.getString("telefono"));
                 System.out.println("");

             }
        }catch (Exception e) {
            System.err.println(e);
        }
        
        //Se crea XML
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        
try{
                         DocumentBuilder builder = factory.newDocumentBuilder();
                        org.w3c.dom.Document document = builder.parse(new File(fichero)); 
                        
                        org.w3c.dom.Element raiz = document.createElement("properties"); //crea la pestaña properties
			document.getDocumentElement().appendChild(raiz);
                        
                        org.w3c.dom.Element elem = document.createElement("entry"); //creaa la pestaña entry
			Text text = document.createTextNode(con.dbms);     //se le da valor
			raiz.appendChild(elem);                          //se agrega el elemeto entry a la raiz 
			elem.appendChild(text);                          //definimos
                        org.w3c.dom.Attr atrr = document.createAttribute("key");  // creamos el atributo key
                        atrr.setValue("dms"); //le asignamos valor
                        elem.setAttributeNode(atrr);  //lo definimos
                        //Se hace lo mismo para cada etiqueta
                        org.w3c.dom.Element elem1 = document.createElement("entry");                        
			Text text1 = document.createTextNode(con.driver);     
			raiz.appendChild(elem1);                          
			elem1.appendChild(text1);
                        org.w3c.dom.Attr atrr1 = document.createAttribute("key");
                        atrr1.setValue("driver");
                        elem1.setAttributeNode(atrr1);
                        
                         org.w3c.dom.Element elem2 = document.createElement("entry");  
			Text text2 = document.createTextNode(con.dbName);     
			raiz.appendChild(elem2);                         
			elem2.appendChild(text2);
                        org.w3c.dom.Attr atrr2 = document.createAttribute("key");
                        atrr2.setValue("database_name");
                        elem2.setAttributeNode(atrr2);
                        
                         org.w3c.dom.Element elem3 = document.createElement("entry");  
                        Text text3 = document.createTextNode(con.userName);   
			raiz.appendChild(elem3);                         
			elem3.appendChild(text3);
                        org.w3c.dom.Attr atrr3 = document.createAttribute("key");
                        atrr3.setValue("user_name");
                        elem3.setAttributeNode(atrr3);
                        
                        org.w3c.dom.Element elem4 = document.createElement("entry");  
                        Text text4 = document.createTextNode("");     
			raiz.appendChild(elem4);                          
			elem4.appendChild(text4);
                        org.w3c.dom.Attr atrr4 = document.createAttribute("key");
                        atrr4.setValue("password");
                        elem4.setAttributeNode(atrr4);
                        
                        org.w3c.dom.Element elem5 = document.createElement("entry");  
                        Text text5 = document.createTextNode(con.serverName);     
			raiz.appendChild(elem5);                          
			elem5.appendChild(text5);
                        org.w3c.dom.Attr atrr5 = document.createAttribute("key");
                        atrr5.setValue("server_name");
                        elem5.setAttributeNode(atrr5);
                        
                        org.w3c.dom.Element elem6 = document.createElement("entry"); 
                        Text text6 = document.createTextNode(String.valueOf(con.portNumber));     
			raiz.appendChild(elem6);                          
			elem6.appendChild(text6);
                        org.w3c.dom.Attr atrr6 = document.createAttribute("key");
                        atrr6.setValue("port_number");
                        elem6.setAttributeNode(atrr6);
                        
                                                Source source = new DOMSource(document); 
			Result result = new StreamResult(new java.io.File(fichero)); 
			Transformer transformer = TransformerFactory.newInstance() .newTransformer(); 
			transformer.transform(source, result);
                        
                        
                        
    
}catch (Exception e) {
			          System.err.println(e);
}
        
        
        


        
        Conexion.closeConnection(cone);

    }
    
}
